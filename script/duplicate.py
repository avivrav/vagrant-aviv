# !/usr/bin/python
import sys
import os
import hashlib
import pprint
import shutil

def dupfiles(dir):
    duplicates = {}
    for dirname, subdirs, fileList in os.walk(dir, topdown=True):
        # print('Scanning %s...' % dirname)
        for name in fileList:
            # pp.pprint (name)
            # path = file path
            path = os.path.join(dirname, name)
            # print (path)
            hashed = hashfile(path)
            # print (hashed)
            if hashed in duplicates:
            # find the duplicate
                duplicates[hashed].append(path)
                # print (duplicates)
            else:
                duplicates[hashed] = [path]
                # print(path)
    # print(duplicates)
    return duplicates


def joinDicts(dict1, dict2):
    for key in dict2.keys():
        if key in dict1:
            dict1[key] = dict1[key] + dict2[key]
        else:
            dict1[key] = dict2[key]

def hashfile(path, blocksize=65536):
   afile = open(path, 'rb')
   hasher = hashlib.md5()
   buf = afile.read(blocksize)
   while len(buf) > 0:
      hasher.update(buf)
      buf = afile.read(blocksize)
   afile.close()
   return hasher.hexdigest()

def calcresult(dict1):
    results = list(filter(lambda x: len(x) > 1, dict1.values()))
    # print('result 1' % results)
    if len(results) > 0:
        for result in results:
            output(results)

    else:
        print('No duplicate files found.')

def output(dupdict):
    # print('Found the following duplicate files:')
    #     low_dest = '/tmp/low_dest'
    #     med_dest = '/tmp/med_dest'
        short = dupdict[0]
        print(','.join([os.path.basename(i) for i in short]))
        # for x in a:
        #     print x
        #     if x.st_size < 50000
        #         shutil.move(x, low_dest)
        #     if x.st_size < 100000
        #         shutil.move(x, med_dest)
        # else
        #     exit()
        #     print tail
            # print tail
        # for x in range(a):
        #     print(x)

if __name__ == '__main__':
    dir = sys.argv[1:]
    pp = pprint.PrettyPrinter(indent=4)
    # dupfiles(dir)
    duplicates = {}
    # check if path exist
    for i in dir:
        # print (i)
        if os.path.exists(i):
            joinDicts(duplicates, dupfiles(i))
        # transfer dict of files with hash to clacresult function
        else:
            # print('%s is not a valid path, please verify' % i)
            sys.exit()
    calcresult(duplicates)